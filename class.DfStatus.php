<?php

require_once('app.config.php');

class DfStatus
{
	function _get_status($ip) 
	{

		$lines = file("http://" . $ip . "/utils/status/dfstatus.php");

		if (preg_match("/Mounted/", $lines[0]))
		{
			$xml = $lines[1];
		} else {
			$xml = "error";
		}

		return $xml;
	}

	function run()
	{
		$result_array = array();

		/*
		$ips = array("89.108.106.12", 
					 "46.19.47.53", 
					 "46.19.47.54", 
					 "89.108.104.178", 
					 "91.221.70.205");

		$srvs = array("Siesta Core (AGAVA)", 
					  "CBS Core", 
					  "CBS Client", 
					  "Vista Server (AGAVA)", 
					  "Pluton Server (Dedic-Center)");

		$limit = array(1, 10, 10, 1, 1);
		*/

		$status = "";
		//$i = 0;

		foreach (SERVER_IPS as $server)
		{
			$output = "";
			$exp = "";

			$output = $this->_get_status($server[0]);

			if ($output == "error") 
			{
				//$i++;
				continue;
			}

			$exp = explode(" ", $output);
			$clear = array_values(array_diff($exp, array("")));
			$output_check = substr($clear[3], 0, -1);

			if ($output_check < $server[2])
			{
				$status  = "WARNING!\n{br}";
				$status .= $server[1] . " (" . $server[0] . ")\n{br}";
				$status .= "Total disk space: " . $clear[1] . "\n{br}";
				$status .= "Disk usage: " . $clear[2] . " (" . $clear[4] . ")\n{br}";
				$status .= "Free disk space: " . $clear[3] . "\n\n{br}{br}";

				$result_array[] = $status;
			}

			//$i++;

		}

		return $result_array;
	}

	function run_once()
	{
		$result_array = array();
		
		/*
		$ips = array("89.108.106.12", 
					 "46.19.47.53", 
					 "46.19.47.54", 
					 "89.108.104.178", 
					 "91.221.70.205");

		$srvs = array("Siesta Core (AGAVA)", 
					  "CBS Core & Admin", 
					  "CBS Client Interface", 
					  "Vista Server (AGAVA)", 
					  "Pluton Server (Dedic-Center)");
		*/

		$status = "";
		$i = 0;
		$check_date = "<i>Check time: " . date("H:i:s d.m.Y") . "</i>";

		foreach (SERVER_IPS as $server)
		{
			$output = "";
			$exp = "";

			$output = $this->_get_status($server[0]);
			$exp = explode(" ", $output);
			$clear = array_values(array_diff($exp, array("")));

			$status = "<b>Server: " . $server[1] . "</b> (" . $server[0] . ")<br/><br/>";
			$status .= "Total disk space: <b>" . $clear[1] . "</b><br/>";
			$status .= "Disk usage: <b>" . $clear[2] . "</b> (" . $clear[4] . ")<br/>";
			$status .= "Free disk space: <b>" . $clear[3] . "</b><br/><br/><br/>";


			//$output = $this->_get_status($ip);
			//$exp = explode(" ", $output);
			//$clear = array_values(array_diff($exp, array("")));

			//$status = "<b>Server: " . $srvs[$i] . "</b> (" . $ip . ")<br/><br/>";
			//$status .= "Total disk space: <b>" . $clear[1] . "</b><br/>";
			//$status .= "Disk usage: <b>" . $clear[2] . "</b> (" . $clear[4] . ")<br/>";
			//$status .= "Free disk space: <b>" . $clear[3] . "</b><br/><br/><br/>";

			$result_array[] = $status;
			$i++;

		}
		$result_array[] = $check_date;

		return $result_array;
	}
}