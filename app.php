<?php
date_default_timezone_set('Asia/Almaty');
//error_reporting(0);
error_reporting(E_ALL);


require "class.BufferStatus.php";
require "class.DfStatus.php";
require "class.IpmStatus.php";
require "class.SysStatus.php";
require "class.RepStatus.php";
require "class.MnpStatus.php";
require "class.Sender.php";
require "class.MysqlStatus.php";
require "class.KbsStatus.php";
require "class.PhpStatus.php";
require "class.SMSStatus.php";
require "class.IVRStatus.php";

$dfstatus = new DfStatus;
$dfresult = $dfstatus->run();

$ipmstatus = new IpmStatus;
$ipmresult = $ipmstatus->run();

$sysstatus = new SysStatus;
$sysresult = $sysstatus->run();

$repstatus = new RepStatus;
$represult = $repstatus->run();

$mnpstatus = new MnpStatus;
$mnpresult = $mnpstatus->run();

$mysqlstatus = new MysqlStatus;
$mysqlresult = $mysqlstatus->run(); 

$phpstatus = new PhpStatus;
$phpresult = $phpstatus->run(); 

$smsstatus = new SMSStatus;
$smsresult = $smsstatus->run(); 

$ivrstatus = new IVRStatus;
$ivrresult = $ivrstatus->run(); 

//print_r($mysqlstatus);
//die;

$sender = new Sender;


//////////////////////////////// BUFFER ////////////////////////////////////
$bfstatus = new BufferStatus;
$bfresult = $bfstatus->run();

if (date("H") == "00" && count($bfresult) > 0)
{
	$subject = "Check buffer size!";
	$text = "";

	foreach ($bfresult as $result) {
		$text .= $result;
	}

	$sender->send_email($subject, $sender->ebr($text));

	if (date("H") > '09' && date("H") < '19')
	{
		//$sender->send_sms('77018866444', $sender->sbr($text)); //Aybek
		//$sender->send_sms('77013698226', $sender->sbr($text)); //Ildar	
		//$sender->send_sms('77017134618', $sender->sbr($text)); //Nurlan	
	}
}


//////////////////////////// DF CHECKER  ///////////////////////////////////
if (date("i") == "00")
{
	if (count($dfresult) > 0)
	{
		$subject = "Check diskspace!";
		$text = "";

		foreach ($dfresult as $result) {
			$text .= $result;
		}

		$sender->send_email($subject, $sender->ebr($text));
		
		//$sender->send_sms('77018866444', $sender->sbr($text));	
		//$sender->send_sms('77013698226', $sender->sbr($text));	
		//$sender->send_sms('77017134618', $sender->sbr($text));	
	}
}

////////////////////////// IPM QUEUE ///////////////////////////////////////
if (count($ipmresult) > 0)
{
	$subject = "Check IPM queue!";
	$text = "";

	foreach ($ipmresult as $result) {
		$text .= $result;
	}

	$sender->send_email($subject, $sender->ebr($text));
	
	//$sender->send_sms('77018866444', $sender->sbr($text));	
	//$sender->send_sms('77013698226', $sender->sbr($text));	
	//$sender->send_sms('77017134618', $sender->sbr($text));	
}

///////////////////////// MYSQL STATUS /////////////////////////////////////
if (count($mysqlresult) > 0)
{
        $subject = "Check MySQL Server!";
        $text = "";

        foreach ($mysqlresult as $result) {
                $text .= $result;
        }

        $sender->send_email($subject, $sender->ebr($text));

        //$sender->send_sms('77018866444', $sender->sbr($text));
        //$sender->send_sms('77013698226', $sender->sbr($text));
        //$sender->send_sms('77017134618', $sender->sbr($text));
}


///////////////////////// PHP STATUS /////////////////////////////////////
if (count($phpresult) > 0)
{
        $subject = "Check PHP FPM Service!";
        $text = "";

        foreach ($phpresult as $result) {
                $text .= $result;
        }

        $sender->send_email($subject, $sender->ebr($text));

        //$sender->send_sms('77018866444', $sender->sbr($text));
        //$sender->send_sms('77013698226', $sender->sbr($text));
        //$sender->send_sms('77017134618', $sender->sbr($text));
}


//////////////////// KANNEL & PROVIDERS /////////////////////////////////////
if (count($sysresult) > 0)
{
	$subject = "Siesta Server Monitoring";
	$text = "";

	foreach ($sysresult as $result) {
		$text .= $result;
	}

	$sender->send_email($subject, $sender->ebr($text));
	
	//$sender->send_sms('77018866444', $sender->sbr($text));	
	//$sender->send_sms('77013698226', $sender->sbr($text));	
	//$sender->send_sms('77017134618', $sender->sbr($text));
}

///////////////////////////// KBS ///////////////////////////////////////////

	$kbsstatus = new KbsStatus;
	$kbsresult = $kbsstatus->run();

	if ($kbsresult == "error")
	{
		$subject = "Siesta Server Monitoring";
		$text = "Check KBS (Kcell Billing micro-Service) ASAP.";

		$sender->send_email($subject, $sender->ebr($text));
		
		//$sender->send_sms('77018866444', $sender->sbr($text));	
		//$sender->send_sms('77013698226', $sender->sbr($text));	
		//$sender->send_sms('77017134618', $sender->sbr($text));
	}

	if ($kbsresult == "fixed")
	{
		$subject = "Siesta Server Monitoring";
		$text = "KBS (Kcell Billing micro-Service) has been restored.";

		$sender->send_email($subject, $sender->ebr($text));
		
		//$sender->send_sms('77018866444', $sender->sbr($text));	
		//$sender->send_sms('77013698226', $sender->sbr($text));	
		//$sender->send_sms('77017134618', $sender->sbr($text));
	}


//////////////////////// DAILY DF NOTIFICATION //////////////////////////////

if (date("Hi") == "1500")
{
	$dforesult = $dfstatus->run_once();
	$subject = "RGL Servers disk space status";
	$text = "";

	foreach ($dforesult as $result) {
		$text .= $result;
	}

	printf("%s\n%s\n", $subject, $text);

	$sender->send_email($subject, $text);
}

///////////////////////// SMS STATUS /////////////////////////////////////
if (count($smsresult) > 0)
{
	if (date("H") > '08' && date("H") < '22')
	{
        $subject = "Check Operators!";
        $text = "";

        foreach ($smsresult as $result) {
                $text .= $result;
        }

        $sender->send_email($subject, $sender->ebr($text));
	}
}

///////////////////////// IVR STATUS /////////////////////////////////////
if (count($ivrresult) > 0)
{
        $subject = "Check Operators!";
        $text = "";

        foreach ($ivrresult as $result) {
                $text .= $result;
        }

        $sender->send_email($subject, $sender->ebr($text));
}

////////////////////////////////// MNP ////////////////////////////////////////
/*
if (date("i") == "25" OR date("i") == "55")
{
	// MNP status
	if ($mnpresult != "ok")
	{
		$subject = "Siesta MNP Monitoring";
		$text = $mnpresult;

		//$sender->send_email($subject, $sender->ebr($text));
		
		$sender->send_sms('77018866444', $sender->sbr($text));	
		//$sender->send_sms('77013698226', $sender->sbr($text));	
		//$sender->send_sms('77017134618', $sender->sbr($text));
	}
}
*/


///////////////////////// Replication (VZ103) stat /////////////////////////////
/*
if (count($represult) > 0)
{
	
	if (trim($represult[0]) != 'Yes' || trim($represult[1]) != 'Yes')
	{

		$subject = "Check replication status!";
		$text = "WARNING!\n{br}Check replication on Vista Server:\n{br}Slave_IO_Running: " . trim($represult[0]) . "\n{br}Slave_SQL_Running: " . trim($represult[1]);

		$sender->send_email($subject, $sender->ebr($text));
		
		$sender->send_sms('77018866444', $sender->sbr($text));	
		$sender->send_sms('77013698226', $sender->sbr($text));	
		// $sender->send_sms('77017134618', $sender->sbr($text));
	}

	//echo "Turn ON Rep status checker!\n";
}
*/


?>
