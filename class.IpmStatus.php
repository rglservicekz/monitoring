<?php

require_once('app.config.php');

class IpmStatus
{
	function _get_status($ip) 
	{
		$lines = file("http://" . $ip . "/utils/status/ipmstatus.php");
		//$lines = file("http://91.221.70.205/utils/status/ipmstatus.php");
		return $lines[0];
	}

	function run()
	{
		$result_array = array();

		$ips = array(MASTER_HOST);

		$srvs = array("Siesta Core IPM (AGAVA)");

		$limit = array(100);

		$status = "";
		$i = 0;

		foreach ($ips as $ip)
		{
			$output = "";
			$exp = "";

			$output = $this->_get_status($ip);

			if ($output == "error") 
			{
				$i++;
				continue;
			}

			$output_check = trim($output);


			if ($output_check > $limit[$i])
			{
				$status  = "WARNING!\n{br}";
				$status .= $srvs[$i] . " (" . $ip . ")\n{br}";
				$status .= "Payments queue size: " . $output_check . "\n{br}{br}";

				$result_array[] = $status;
			}

			$i++;

		}

		return $result_array;
	}
}