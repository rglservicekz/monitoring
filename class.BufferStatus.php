<?php

require_once('app.config.php');

class BufferStatus 
{

	function _get_status($ip) {

		$lines = file("http://" . $ip . "/utils/status/bufferstatus.php");

		$xml = $lines[0];
		return $xml;
	}

	function run()
	{
		$result_array = array();

		$ips = array(MASTER_HOST);
		$srvs = array("Siesta Core (Agava)");
		$limit = array(150);

		$status = "";
		$i = 0;

		foreach ($ips as $ip)
		{
			$output = "";
			$exp = "";

			$output = $this->_get_status($ip);
			//$output = 100;

			if ($output == "error") 
			{
				$i++;
				continue;
			}

			if ($output > $limit[$i])
			{

				$status  = "TEST WARNING!\n{br}";
				$status .= $srvs[$i] . " (" . $ip . ")\n{br}";
				$status .= "Buffer size: " . $output . "\n{br}";
				$status .= "Check time: " . date("H:i:s d.m.Y");

				$result_array[] = $status;
			}

			$i++;

		}

		return $result_array;
	}
}

?>
