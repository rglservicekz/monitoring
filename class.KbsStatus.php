<?php

require_once('app.config.php');

class KbsStatus
{
	function _get_status($ip) 
	{

		$lines = file("http://" . $ip . ":3337/api/billing?phone=77018866444");
		$json = $lines[0];

		return $json;
	}

	function run()
	{

		$status = "";
		$last_status = "ok";
		$final_result = "ok";
		$ip = MASTER_HOST;

		$output = $this->_get_status($ip);
		$check_result = json_decode($output); 

			if ($check_result->code == 200) 
			{
				$result = "ok";
			} else {
				$result = "error";
			}

			$last_status = file_get_contents("kbs.log");

			//echo $last_status . "1file\n";

			////////////////////////////
			$fd = fopen("kbs.log", 'w');
			////////////////////////////

			if ( ($last_status == "error" OR $last_status == "error_more") AND $result == "ok" ) {
				$final_result = "fixed";
				fwrite( $fd, $final_result );
			} else if ( $last_status == "ok" AND $result == "error" ) {
				$final_result = "error";
				fwrite( $fd, $final_result );
			} else if ( ($last_status == "error" OR $last_status == "error_more") AND $result == "error" ) {
				$final_result = "error_more";
				fwrite( $fd, $final_result );
			} else {
				$final_result = "ok";
				fwrite( $fd, $final_result );
			}

			///////////////////////////
			fclose($fd);
			///////////////////////////

			//echo $final_result . "2finalresult\n";

		return $final_result;
	}

}