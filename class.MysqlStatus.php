<?php

require_once('app.config.php');

class MysqlStatus
{
	function _get_status($ip) 
	{

		$lines = file("http://" . $ip . "/utils/status/mysqlstatus.php");
		//print_r($lines);
		return $lines[0];
	}

	function run()
	{
		$result_array = array();

		$ips = array(MASTER_HOST);
		$srvs = array("Siesta Core MySQL (AGAVA)");


		$status = "";
		$i = 0;

		foreach ($ips as $ip)
		{
			$output = "";
			$exp = "";
			
			$output = $this->_get_status($ip);			

			//print_r($output);

			if (trim($output) != "OK" )
			{
				$status  = "WARNING!\n{br}";
				$status .= $srvs[$i] . " (" . $ip . ")\n{br}";
				$status .= "Check Mysql server asap (Error: Cannot connect to  Mysql).\n{br}{br}";

				$result_array[] = $status;
			}

			$i++;

		}

		return $result_array;
	}
}
