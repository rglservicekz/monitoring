<?php

date_default_timezone_set('Asia/Almaty');
//error_reporting(0);
error_reporting(E_ALL);

require "class.Sender.php";
require "class.IVRStatus.php";

$ivrstatus = new IVRStatus;
$ivrresult = $ivrstatus->run(); 

$sender = new Sender;

///////////////////////// IVR STATUS /////////////////////////////////////
if (count($ivrresult) > 0)
{
        $subject = "Check Operators!";
        $text = "";

        foreach ($ivrresult as $result) {
                $text .= $result;
        }

        $sender->send_email($subject, $sender->ebr($text));
}

?>
