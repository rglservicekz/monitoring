<?php

class RepStatus
{
	function _get_status() 
	{

		$lines = file('http://vista.rglservice.kz/tools/monitoring/rep_status.log');

		return $lines;
	}

	function run()
	{
		$result_array = array();

		$output = $this->_get_status();
		
		$check1 = explode(':', str_replace(' ', '', $output[11]));
		$check2 = explode(':', str_replace(' ', '', $output[12]));

		$result_array[] = trim($check1[1]);
		$result_array[] = trim($check2[1]);

		return $result_array;
	}
}