<?php
require_once('app.config.php');
require_once('mailer/class.phpmailer.php');

class Sender
{
	function ebr($text)
	{
		$result = str_replace('{br}', '<br/>', $text);
		return $result;
	}

	function sbr($text)
	{
		$result = str_replace('{br}', '', $text);
		return $result;
	}

	function send_sms($phone, $text)
	{
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, 'http://smsc.ru/sys/send.php?login=rglservice&psw=rgl2458897&phones='.$phone.'&mes=' . urlencode($text));
	    curl_setopt($ch, CURLOPT_HEADER, false);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
	    $data = curl_exec($ch);
	    curl_close($ch);
	}

	function send_email($subject, $text)
	{
		$mail = new PHPMailer(true);
		$mail->IsSMTP();

		$mail->Host       	= "smtp.googlemail.com";
		$mail->SMTPDebug  	= 1;
		$mail->SMTPAuth   	= true;
		$mail->SMTPSecure 	= "ssl";
		$mail->Host       	= "smtp.googlemail.com";
		$mail->Port       	= 465;
		$mail->Username   	= MAIL_USERNAME;
		$mail->Password   	= MAIL_PASSWORD;

		$mail->AddReplyTo(MAIL_USERNAME, 'RGL Monitoring System');

/*		$mail->AddAddress('it@rglservice.kz', 'RGL Service IT Dep.');
		$mail->AddAddress('ildar@rgl.kz', 'RGL Service IT Dep.');
		$mail->AddAddress('bernur@rgl.kz', 'RGL Service IT Dep.');		*/

		foreach (MAIL_ADDRESSES as $address) {
			$mail->AddAddress($address[0], $address[1]);
			//fwrite(STDOUT, $address[0] . ' ' . $address[1]);
		}

		$mail->SetFrom(MAIL_USERNAME, 'RGL Monitoring System');

		$mail->Subject 		= $subject;
	    $mail->AltBody 		= $subject;
	    $mail->MsgHTML("<html><body>" . $text . "</body></html>");
	    $mail->Send();
	}
}