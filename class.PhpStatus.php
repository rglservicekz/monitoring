<?php

require_once('app.config.php');

class PhpStatus
{
	function _get_status($ip) 
	{
		$headers = get_headers("http://" . $ip . "/status");
		list($proto, $code, $descr) = explode(' ', $headers[0], 3);


		return $code;
	}

	function run()
	{
		$result_array = array();

		$ips = array(MASTER_HOST, "89.108.104.178");
		$srvs = array("Pluton Server (Dedic-Center)", "Vista Server (AGAVA)");


		$status = "";
		$i = 0;

		foreach ($ips as $ip)
		{
			$output = "";
			$exp = "";
			
			$output = $this->_get_status($ip);			

			//print_r($output);

			if ($output != 200 )
			{
				$status  = "WARNING!\n{br}";
				$status .= $srvs[$i] . " (" . $ip . ")\n{br}";
				$status .= "Check PHP FPM service asap (Error: Cannot retrieve status of the FPM).\n{br}{br}";

				$result_array[] = $status;
			}

			$i++;

		}

		return $result_array;
	}
}
