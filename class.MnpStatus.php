<?php

require_once('app.config.php');

class MnpStatus
{
	function _get_status($ip) 
	{

		$lines = file("http://" . $ip . "/utils/status/mnpdlstatus.php");
		$xml = $lines[0];

		return $xml;
	}

	function run()
	{
		$result_array = array();
		$ips = array(MASTER_HOST);

		$status = "";
		$i = 0;

		foreach ($ips as $ip)
		{
			$output = "";
			$exp = "";

			$output = $this->_get_status($ip);

			if ($output == "ok") 
			{
				$result = "ok";
				//$i++;
				//continue;
			} else {
				$result = $output;
			}

			$i++;

		}

		return $result;
	}

}