<?php

require_once('app.config.php');

class SysStatus
{

	function run()
	{
		$result_array = array();

		$ip         = MASTER_HOST;
		$port       = KANNEL_PORT;

		$user       = MONITOR_USER;
		$password   = MONITOR_PASSWORD;
		$database   = MONITOR_DB;
		$server     = MONITOR_HOST;

		$conn       = mysql_connect($server, $user, $password) or die ("Connection error!");
		$db         = mysql_select_db($database, $conn);
		$set        = mysql_query("SET NAMES 'utf8'");

		$checkport = fsockopen( $ip, $port, $errno, $errstr );
		//$checkport = false;
		
		if (!$checkport) {
		    $result = "error";

		    $rquery = "INSERT INTO `server_status` (`ipaddr`, `status`, `check_time`) VALUES ('".$ip."', '".$result."', '".time()."')";
		    mysql_query($rquery);

		} else {
		    $result = "connected";

		    $rquery = "INSERT INTO `server_status` (`ipaddr`, `status`, `check_time`) VALUES ('".$ip."', '".$result."', '".time()."')";
		    mysql_query($rquery);

		    ///////////////////////////////////////////
		    $xml = simplexml_load_file('http://'.$ip.':'.$port.'/status.xml?&password=admin');

		    for ($i=0; $i<$xml->smscs->count; $i++)
		    {
		        $statusexp = explode(" ", $xml->smscs->smsc[$i]->status);
		        $status = trim($statusexp[0]);

		        if ($status == "online") 
		        {
		            $status = "online";
		        } else {
		            $status = "offline";    
		        }

		        // $status = "offline"; 

		        $query = "INSERT INTO `operator_status` (`kannel_id`, `status`, `check_time`) VALUES ('".$xml->smscs->smsc[$i]->id."', '".$status."', '".time()."')";
		        mysql_query($query);
		    }
		}
		sleep (3);


		// Send Notification if Operator|s offline
		$aResult    = "";

		$lquery = "SELECT * FROM `kannel_id_list` ORDER BY `id` ASC";
		$list = mysql_query($lquery);
		$list_rows = mysql_num_rows($list);

		for ($q=0;$q<$list_rows;$q++)
		{
		    $listObj = mysql_fetch_object($list);

		    // 27.08.2015 - Turn off Beeline
		    //if (date("dm") == '2708' && $listObj->id == 16) {
		    //	continue;
		    //}

		    // 11.08.2015 - Turn off Tele2
		    //if (date("dm") == '1108' && $listObj->id == 1) {
		    //	continue;
		    //}

		    $cquery = "SELECT * FROM `operator_status` WHERE `kannel_id` LIKE '".$listObj->kannel_id."' ORDER BY `id` DESC LIMIT 0, 3"; 
		    $check_conn = mysql_query($cquery);
		    $check_rows = mysql_num_rows($check_conn);

		    $check_arr = array();
		    for ($i=0; $i<$check_rows; $i++)
		    {
		        $cObj = mysql_fetch_object($check_conn);
		        array_push($check_arr, $cObj->status);    
		    }

		    $oup_arr = array('online', 'offline', 'offline');
		    $odown_arr = array('offline', 'offline', 'online'); 

		    if ($check_arr == $odown_arr) {
		        
		        $coq = "SELECT * FROM `operator_status` WHERE `kannel_id` LIKE '".$listObj->kannel_id."' ORDER BY `id` DESC LIMIT 3, 1"; 
		        $coq_obj = mysql_fetch_object(mysql_query($coq));

		        if ($coq_obj->status == "offline") {
		            continue;
		        } else {
		            $Result = ucfirst(substr($listObj->kannel_id, 5)) . ": Offline.\n{br} ";
		        }

		    } else if ($check_arr == $oup_arr) {
		        
		        $Result = ucfirst(substr($listObj->kannel_id, 5)) . ": Online.\n{br} ";

		    } else {
		        $Result = "";
		    } 

		    $aResult .= $Result;   
		}

		if ($aResult != "" && $aResult != " " && !empty($aResult))
		{
		    $result_array[] = $aResult;
		} 
		sleep (1);


		// Send Notifcation if Server cannot be resolved
		$cquery = "SELECT * FROM `server_status` WHERE `ipaddr` LIKE '" . $ip . "' ORDER BY `check_time` DESC LIMIT 0, 3"; 
		$check_conn = mysql_query($cquery);
		$check_rows = mysql_num_rows($check_conn);

		$check_arr = array();
		for ($i=0; $i<$check_rows; $i++)
		{
		    $cObj = mysql_fetch_object($check_conn);
		    array_push($check_arr, $cObj->status);    
		}

		$mcup_arr = array('connected', 'error', 'error');
		$mcdown_arr = array('error', 'error', 'error');

		if ($check_arr == $mcdown_arr)
		{
	        $Result = "Connection error: Kannel (" . $ip . ") is unavailable. Check ASAP!!!";

	        $result_array[] = $Result;

		} else if ($check_arr == $mcup_arr)
		{
		    $Result = "Connection resumed: Kannel (" . $ip . ") is available. Please check transactions & mobile operators, to be sure!";

		    $result_array[] = $Result;
		}

		//var_dump($result_array);
		//fwrite(STDOUT, ob_get_clean());

		return $result_array;
	}
}
