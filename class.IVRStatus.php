<?php

require_once('app.config.php');

class IVRStatus
{

	function run()
	{
		$result_array = array();

		if (date("i") != "00") { //!!

			$ip         = MASTER_HOST;
			$port       = KANNEL_PORT;

			$user       = MONITOR_USER;
			$password   = MONITOR_PASSWORD;
			$databaseCBS   		= CBSCORE_DB;
			$databaseMonitor  	= MONITOR_DB;
			$server     = MONITOR_HOST;

			$conn    	= mysql_connect($server, $user, $password) or die ("Connection error!");

			mysql_select_db($databaseMonitor);
			mysql_query("SET NAMES 'utf8'");
			mysql_select_db($databaseCBS);
			mysql_query("SET NAMES 'utf8'");

		    $aResult 	= "";
			
			$rquery = "SELECT MIN(`id`) AS `min`, MAX(`id`) AS `max` FROM (SELECT * FROM `bin` WHERE `is_parsed` = 1 ORDER BY `id` DESC LIMIT 0, 4) AS `minmax`";
			$range = mysql_query($rquery);
		    $rObj = mysql_fetch_object($range);

			// echo "Start bin_id = " . $rObj->min . "\n";
			// echo "Stop bin_id = " . $rObj->max . "\n";
			// fwrite(STDOUT, ob_get_clean());

		    if (!(is_null($rObj->min) || is_null($rObj->max))) {
		    	//------------- Тишина по операторам
				$lquery = "SELECT * FROM `operator` ORDER BY `id` ASC";
				$list = mysql_query($lquery);
				$list_rows = mysql_num_rows($list);

				for ($q=0;$q<$list_rows;$q++)
				{
				    $listObj = mysql_fetch_object($list);

					$Result = "";
				    $cquery = "SELECT SUM(`maxchan`) AS `sum` FROM `bin_stats` WHERE `bin_id` >= ".$rObj->min." AND `bin_id` <= ".$rObj->max." AND `oid` = ".$listObj->id; 
				    $check_conn = mysql_query($cquery);
				    $cObj = mysql_fetch_object($check_conn);
				    
				    if (!is_null($cObj->sum)) {
					    // Определяем что не было ни одного звонка по данному оператору
					    if ($cObj->sum == 0) {
				            $Result = $listObj->name . ": no calls at last hour.\n{br} ";
					    }
				    } else {
				    	$Result = $listObj->name . ": Fatal error SUM function returned NULL!.\n{br} ";	
				    }

					// echo $listObj->name . " total = " . $cObj->sum . "\n";
					// fwrite(STDOUT, ob_get_clean());

			        $aResult .= $Result;
		        }
				
				//------------- Тишина по клиентам
				mysql_select_db($databaseMonitor);
	
				$lquery = "SELECT * FROM `client_vip` ORDER BY `id` ASC";
				$list = mysql_query($lquery);
				$list_rows = mysql_num_rows($list);

				mysql_select_db($databaseCBS);	
				for ($q=0;$q<$list_rows;$q++)
				{
				    $listObj = mysql_fetch_object($list);

					$Result = "";
				    $cquery = "SELECT SUM(`maxchan`) AS `smaxchan`, SUM(`billsec_c`) AS `sbillsecc`,  SUM(`billsec_o`) AS `sbillseco` FROM `bin_stats` WHERE `bin_id` >= ".$rObj->min." AND `bin_id` <= ".$rObj->max." AND `cid` = ".$listObj->id; 

					// echo "cquery = " . $cquery . "\n";
					// fwrite(STDOUT, ob_get_clean());

				    $check_conn = mysql_query($cquery);
				    $cObj = mysql_fetch_object($check_conn);
				    
					// echo "client = " . $listObj->name . ", smaxchan = " . $cObj->smaxchan . ", sbillsecc = " . $cObj->sbillsecc .", sbillseco = " . $cObj->sbillseco ."\n";
					// fwrite(STDOUT, ob_get_clean());

				    if (!is_null($cObj->smaxchan)) {
					    // Определяем что не было ни одного звонка по данному клиенту
					    if ($cObj->smaxchan == 0) {
				            $Result = $listObj->name . ": no calls at last hour.\n{br} ";
					    } else {
						    if ($cObj->sbillseco == 0) {
					            $Result = $listObj->name . ": zero operator billing at last hour.\n{br} ";
						    } else {
						    	$diff = $cObj->sbillsecc / $cObj->sbillseco;

								// echo "diff = " . $diff . "\n";
								// fwrite(STDOUT, ob_get_clean());
						    	if ($diff < 0.5) {
						    		$Result = $listObj->name . ": client billing sec too lower than operator (billsec_c = ". $cObj->sbillsecc .", billsec_o = ". $cObj->sbillseco .").\n{br} ";	
						    	}
						    }

					    }
				    } else {
				    	$Result = $listObj->name . ": Fatal error SUM(maxchan) function returned NULL!.\n{br} ";	
				    }

					// echo $listObj->name . " total = " . $cObj->sum . "\n";
					// fwrite(STDOUT, ob_get_clean());

			        $aResult .= $Result;
		        }

		    } else {
		    	$aResult .= "Fatal error: min(bin_id) or max(bin_id) is NULL!.\n{br} ";	
		    }

			
			if ($aResult != "" && $aResult != " " && !empty($aResult))
			{
			    $result_array[] = $aResult;
			} 
		} 
		return $result_array;
	}
}
