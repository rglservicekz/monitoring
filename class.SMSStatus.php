<?php

require_once('app.config.php');

class SMSStatus
{

	function run()
	{
		$result_array = array();

		if (date("i") == "00") {

			$ip         = MASTER_HOST;
			$port       = KANNEL_PORT;

			$user       = MONITOR_USER;
			$password   = MONITOR_PASSWORD;
			$database   = MONITOR_DB;
			$server     = MONITOR_HOST;

			$conn       = mysql_connect($server, $user, $password) or die ("Connection error!");
			$db         = mysql_select_db($database, $conn);
			$set        = mysql_query("SET NAMES 'utf8'");

			$smsstatus  = ['tele2' => [0, 0], 'altel' => [0, 0], 'kcell' => [0, 0], 'beeline' => [0, 0]];

		    $aResult 	= "";

			$checkport = fsockopen( $ip, $port, $errno, $errstr );
			
			if ($checkport) {
			    $xml = simplexml_load_file('http://'.$ip.':'.$port.'/status.xml?&password=admin');

			    for ($i=0; $i<$xml->smscs->count; $i++)
			    {
			        $kannel_id = explode(" ", $xml->smscs->smsc[$i]->id);
			        $received = explode(" ", $xml->smscs->smsc[$i]->sms->received);
			        $sent = explode(" ", $xml->smscs->smsc[$i]->sms->sent);

			        foreach ($smsstatus as $key => $value) {
				        if (!(strpos($kannel_id[0], $key) === false))
				        {
				        	$smsstatus[$key][0] = $smsstatus[$key][0] + $received[0];
				        	$smsstatus[$key][1] = $smsstatus[$key][1] + $sent[0];
				        }
			        }

			    }
				//var_dump($smsstatus);
				//fwrite(STDOUT, ob_get_clean());

		        foreach ($smsstatus as $key => $value) {
					$Result = "";
				    $cquery = "SELECT * FROM `sms_status` WHERE `operator` LIKE '".$key."' ORDER BY `id` DESC LIMIT 0, 1"; 
				    $check_conn = mysql_query($cquery);
				    $check_rows = mysql_num_rows($check_conn);

				    if ($check_rows > 0) {
					    $cObj = mysql_fetch_object($check_conn);
				
					    $period = time() - $cObj->check_time;
						//echo "period = " . $period . "\n";
						//fwrite(STDOUT, ob_get_clean());

					    // Определяем что прошел час, точнее больше 50 мин
					    if ($period / 60 > 50) {
						    // Если последнее число принятых и отправленных СМС не изменилось, то сигнализируем об этом
						    if (!($value[0] > $cObj->received)) {
					            $Result = ucfirst($key) . ": SMS received count not changed at last hour.\n{br} ";
						    }
						    if (!($value[1] > $cObj->sent)) {
					            $Result .= ucfirst($key) . ": SMS sent count not changed at last hour.\n{br} ";
						    }
			    	    }
				    }

			        $query = "INSERT INTO `sms_status` (`operator`, `received`, `sent`, `check_time`) VALUES ('".$key."', '".$value[0]."', '".$value[1]."', '".time()."')";
			        mysql_query($query);

			        $aResult .= $Result;
				
					//echo "aResult = " . $aResult . "\n";
					//fwrite(STDOUT, ob_get_clean());
		        }
			}
			//sleep (3);

			if ($aResult != "" && $aResult != " " && !empty($aResult))
			{
			    $result_array[] = $aResult;
			} 

			//var_dump($result_array);
			//fwrite(STDOUT, ob_get_clean());

		} //END if (date("i") == "00")
		return $result_array;
	}
}
